var extremist1 = "";
var extremist2 = "";
var extremist3 = "";
var extremist4 = "";
var twitter_user = "";

function searchTwitterUser() {
    var user_name= document.getElementById("userInput").value;
    generateUser(user_name);

}

function generateUser(user) {
    twitter_user = user;
    // var handle = user;
    // var container = document.getElementById('profile-pic');
    // var userUrl = "https://twitter.com/"+user+"/profile_image?size=original";
    // document.getElementById("twitter-handle").innerHTML = `@${handle}`;
    // document.getElementById('twitter_handle_title').style.display = 'block';
    // document.getElementById('haterank_result_title').style.display = 'block';
    // console.log(container.src = userUrl);
    // container.src = userUrl;
    // console.log("In Generate User");
    // console.log(user);
    // console.log(userUrl);

    request_stuff('/user/' + user);
}

function searchKeyPress(e){
    // look for window.event in case event isn't passed in
    e = e || window.event;
    if (e.keyCode == 13)
    {
        document.getElementById('search_twitter').click();
        clearSearchBar();
        return false;
    }
    return true;
}

function clearSearchBar() {
    document.getElementById('userInput').value = "";
}

function update_recent_tweet(text) {
    var div = document.getElementById('recent-tweet');
    console.log(div);
    div.innerHTML = text;
}
function update_profile_picture(){
    handle = twitter_user;
    document.getElementById('profile-pic').style.display = 'block';
    document.getElementById('twitter-handle').style.display = 'block';
    var container = document.getElementById('profile-pic');
    var userUrl = "https://twitter.com/"+handle+"/profile_image?size=original";
    document.getElementById("twitter-handle").innerHTML = `@${handle}`;
    container.src = userUrl;


}

function update_most_hateful_tweet(text) {
    var div = document.getElementById('most-hateful-tweet');
    console.log(div);
    div.innerHTML = text;
}

function update_value(text) {
    var div = document.getElementById('result-from-haterank');
    console.log(div);
    div.innerHTML = text;
}

function display_graph(url) {
    var img = document.getElementById('results-graph');
    document.getElementById('results-graph').style.display = 'block';
    img.src = url + '.png'
}

function request_stuff(url) {
    var httpRequest;
    // document.getElementById("ajaxButton").addEventListener('click', makeRequest);

    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
        // alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    
    document.getElementById('profile-pic').style.display = 'none';
    document.getElementById('twitter-handle').style.display = 'none';
    document.getElementById('speech-bubble').style.display = 'none';
    document.getElementById('results-graph').style.display = 'none';

    // this alert() line is where you can display loading stuff.
    // Note: You probably shouldn't use an alert() for this, this is just an example.
    // alert('This is where you should start displaying a loading thingy.');
    document.getElementById('loading_loader').style.display = "block";
    httpRequest.onreadystatechange = alertContents;
    httpRequest.open('GET', url);
    httpRequest.send();

    function alertContents() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                // this is where you should tell it to stop.
                // alert('After you click this, it will display results, this is where you should stop displaying the loading thingy.');
                document.getElementById('loading_loader').style.display = "none";
                var obj = JSON.parse(httpRequest.responseText);
                update_recent_tweet(obj[0]);
                update_most_hateful_tweet(obj[2]);
                update_value(obj[1]);
                display_graph(url);
                update_profile_picture();
                document.getElementById('speech-bubble').style.display = 'block';
                // alert(httpRequest.responseText);
            } else {
                // alert('There was a problem with the request.');
            }
        }
    }
};

function generateRandomUser() {

    var extremist = ["AprilintheNorth","Daniel_Friberg","EliMosleyIE","Henrik_Palmgren","IdentityEvropa","JMcFeels","LanaLokTeff","NathanDamigo","RichardBSpencer"
        ,"SenatorInvictus","TOOEdit","jarTaylor","mikeenochsback","occdissent","redicetv","totalfascism"];

    var max1 = 3;
    var min1 = 0;

    var max2 = 7;
    var min2 = 4;

    var max3 = 11;
    var min3 = 8;

    var max4 = 15;
    var min4 = 12;

    var container1 = document.getElementById('profile-pic1');
    var container2 = document.getElementById('profile-pic2');
    var container3 = document.getElementById('profile-pic3');
    var container4 = document.getElementById('profile-pic4');

    var random1 = Math.floor(Math.random() * (max1 - min1 + 1)) + min1;
    var random2 = Math.floor(Math.random() * (max2 - min2 + 1)) + min2;
    var random3 = Math.floor(Math.random() * (max3 - min3 + 1)) + min3;
    var random4 = Math.floor(Math.random() * (max4 - min4 + 1)) + min4;

    var userUrl1 = "https://twitter.com/"+extremist[random1]+"/profile_image?size=original";
    var userUrl2 = "https://twitter.com/"+extremist[random2]+"/profile_image?size=original";
    var userUrl3 = "https://twitter.com/"+extremist[random3]+"/profile_image?size=original";
    var userUrl4 = "https://twitter.com/"+extremist[random4]+"/profile_image?size=original";

    console.log(userUrl1);

    container1.src = userUrl1;
    container2.src = userUrl2;
    container3.src = userUrl3;
    container4.src = userUrl4;

    extremist1 = extremist[random1];
    extremist2 = extremist[random2];
    extremist3 = extremist[random3];
    extremist4 = extremist[random4];
}

function genUser1() {
    document.getElementById('profile-pic').style.display = 'none';
    document.getElementById('speech-bubble').style.display = 'none';
    document.getElementById('results-graph').style.display = 'none';
    document.getElementById('twitter-handle').style.display = 'none';
    generateUser(extremist1);
}
function genUser2() {
    document.getElementById('profile-pic').style.display = 'none';
    document.getElementById('speech-bubble').style.display = 'none';
    document.getElementById('results-graph').style.display = 'none';
    document.getElementById('twitter-handle').style.display = 'none';
    generateUser(extremist2);
}
function genUser3() {
    document.getElementById('profile-pic').style.display = 'none';
    document.getElementById('speech-bubble').style.display = 'none';
    document.getElementById('results-graph').style.display = 'none';
    document.getElementById('twitter-handle').style.display = 'none';
    generateUser(extremist3);
}
function genUser4() {
    document.getElementById('profile-pic').style.display = 'none';
    document.getElementById('speech-bubble').style.display = 'none';
    document.getElementById('results-graph').style.display = 'none';
    document.getElementById('twitter-handle').style.display = 'none';
    generateUser(extremist4);
}
