import classification.trainer as tr
import classification.classifier as cl
import tweet_downloader as td
import json
from flask import Flask, make_response

app = Flask(__name__, static_folder='HateRankUI')

# Pass in whatever data you need for the graph here.
temp_data = {'test':[0]}

def get_files(filename):
    with open("HateRankUI/" + filename, "r") as f:
        return f.read()

@app.route("/")
def index():
    return get_files("haterank.html")

@app.route("/user/<username>")
def examine_user(username):
    td.get_all_tweets(username)
    temp_tuple = c.classify_user(username)
    global temp_data
    temp_data = temp_tuple[3]
    return json.dumps(temp_tuple[:3])

@app.route("/user/<username>.png")
def display_user_graph(username):
    import StringIO

    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure

    global temp_data

    fig=Figure()
    plt=fig.add_subplot(111)
    
    # the histogram of the data
    n, bins, patches = plt.hist(temp_data, 50, normed=1, facecolor='green', alpha=0.75)

    plt.set_xlabel('HateRank')
    plt.set_title(r'haterank Hist')
    plt.grid()
    
    canvas=FigureCanvas(fig)
    png_output = StringIO.StringIO()
    canvas.print_png(png_output)
    response=make_response(png_output.getvalue())
    response.headers['Content-Type'] = username + '/png'

    # Making sure to clear this when we don't need it anymore.
    temp_data = {'':[]}

    return response

c = cl.classifier()
tr.train_classifier(c)
