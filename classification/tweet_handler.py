import csv

# this is just a simple function to handle a users tweets
def tweet_generator(filename):
    print type(filename)
    #to_return = []
    if isinstance(filename, str):
        print "this is a string?"
        for tweet in csv.reader(open(filename)):
            text = ''.join(tweet[3])
            utf_text = text.decode("utf-8")
            #to_return.append(utf_text.encode("ascii","ignore"))
            yield utf_text.encode("ascii", "ignore")
    if isinstance(filename, unicode):
        print "this is a string?"
        for tweet in csv.reader(open(filename)):
            text = ''.join(tweet[3])
            utf_text = text.decode("utf-8")
            #to_return.append(utf_text.encode("ascii","ignore"))
            yield utf_text.encode("ascii", "ignore")
    elif isinstance(filename, file):
        for tweet in csv.reader(filename):
            text = ''.join(tweet[3])
            utf_text = text.decode("utf-8")
            #to_return.append(utf_text.encode("ascii","ignore"))
            yield utf_text.encode("ascii", "ignore")
