from textblob.classifiers import NaiveBayesClassifier
import tweet_handler
import os.path
import os
import cPickle as pickle
from textblob import TextBlob as tb
from textblob import Word
#import dill
#to add separate feature extractor call the constructor with the feature extractor (is that what they are called in python?) 
class classifier:

    cl = NaiveBayesClassifier([])
    ready = False
    filename = ""



    def new_extractor(document):
        features = {}
        blob = tb(document)
        #misspelled = 0
        for word in blob.words:
            features["contains({0})".format(word)] = True
            #w = Word(word)
            #sp = w.spellcheck()
            #if len(sp) > 0 and 0.5 <= w.spellcheck()[0][1]:
                #misspelled += 1
                                                                                                                    
        features["sentiment_pol"] = blob.sentiment.polarity
        features["sentiment_sub"] = blob.sentiment.subjectivity
        #features["misspellings"] = misspelled
        return features


    def __init__(self,name = "default_classifier", feature_extractor = new_extractor):
        self.filename = name+".p"
        print "exist and isfile", os.path.exists(self.filename) and os.path.isfile(self.filename)
        if os.path.exists(self.filename) and os.path.isfile(self.filename):
            print "loading from picle file:" + name
            self.cl = pickle.load(open(self.filename,"r+"))
            self.ready = True

        else:
            print "default file does note exist creating default file"
            self.ready = False
            if feature_extractor is None:
                self.cl = NaiveBayesClassifier([("test","neg")])
            else:
                self.cl = NaiveBayesClassifier([],feature_extractor)

    def __prep_data(self,filename,extreme):
        tweets = tweet_handler.tweet_generator(filename)
        for text in tweets:
            if extreme:
                yield (text, 'pos')
                #l.append((text,'pos'))
            else:
                yield (text, 'neg')
                #l.append((text,'neg'))

    def train(self, filename, extreme):
        print "starting training"
        datas = []
        for i, datum in enumerate(self.__prep_data(filename,extreme)):
            if i > 1000:
                break
            datas.append(datum)
        self.cl.update(datas)
        self.ready = True

    def save(self):
        print "no save"
        #dumps(self.cl,open(self.filename,"w+"))

    def classify(self, f):
        if not self.ready:
            print "not ready yet. Use train(file) to train."
            return
        prob = self.cl.prob_classify(f)
        #print f
        #print prob.max(), prob.prob(prob.max())
        #return prob.max(), prob.prob(prob.max())
        return prob

    def info(self):
        if not self.ready:
            print "not ready yet. Use train(file) to train."
            return
        self.cl.show_informative_features(10)

    def classify_user(self, u):
        print "classifing user"
        filename = u + "_tweets.csv"
        max_pos_tweet = [0, '']
        data_dict = {
                'pos':[],
                'neg':[],
                'hist':[]
        }
        print os.path.exists(filename)
        max_hate = -2
        for i, datum in enumerate(tweet_handler.tweet_generator(filename)):
            if i == 1:
                tweet = datum
            if i > 3000:
                break
            
            prob = self.classify(datum)
            direction = prob.max()
            value = prob.prob(prob.max())
             
            data_dict[direction].append(value)
            data_dict['hist'].append(prob.prob('pos'))
            #data_dict['hist'].append(prob.prob('neg'))
            if prob.prob('pos') > max_hate:
                max_hate = value
                max_pos_tweet = "\"" + datum + "\" has a hate value of: " + str(value)
        possum = sum(data_dict['pos'])
        negsum = sum(data_dict['neg'])
        if negsum == 0:
            negsum = 0.001
        haterank = possum/negsum
        is_extreme = "not Extremeist: " + str(haterank)
        if haterank > 8:
            is_extreme = 'Extremist: ' + str(haterank)
        toreturn = (tweet, is_extreme, max_pos_tweet, data_dict['hist'])
        #make_hist.make_hist(data_dict['hist'])
        print "tweet", tweet
        print "isextreme", is_extreme 
        print "max_pos", max_pos_tweet
        return toreturn
