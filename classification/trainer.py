import classifier
import os
import cPickle as pickle

def train_classifier(classif):
    directory_extreme = "test_data/"
    directory_non = "non_extremist_data/"
    trained = []
    trained_name = "default_trainer.p"
    if os.path.exists(trained_name):
        trained = pickle.load(open(trained_name,"rb")) 
    numb = 13

    for filename in os.listdir(directory_extreme)[:numb]:
        if(filename not in trained):
            print "training with user:" + filename
            trained.append(filename)
            classif.train(open(directory_extreme+filename),True)
            os.system("free")
            
    for filename in os.listdir(directory_non)[:numb]:
        if(filename not in trained):
            print "training with user:" + filename
            trained.append(filename)
            classif.train(open(directory_non+filename),False)
            os.system("free")

    classif.save()
    print "The following are the files that have been trainded"
    for filename in trained:
        print  filename
    #pickle.dump( trained, open(trained_name, "wb"))
    classif.info()

