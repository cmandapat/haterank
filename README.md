# HateRank #

### Project proposal for Cameron Alexander, Henry Ramsey Bissex, Alex Huddleston, Christian Mandapat ###

HateRank aims to be a way of identifying extremists by analyzing the tweets, retweets, and interactions of some known extremists and comparing it to other users.

## Setup ##

Here is a full list of what you will need for setup.

### Installing natively ###

You will need to have installed Python 2 in your environment path, along with having the Tweepy module in your
environment.

First, you will want to grab pip if you are setting up through your native environment. A quick way to do this is to follow hte instructions through this link: https://pip.pypa.io/en/stable/installing/

Here are the list of things you'll need to install:

```
$ pip install flask
$ pip install tweepy
$ pip install matplotlib
$ pip install -U textblob
$ python -m textblob.download_corpora
```

### Installing through Anaconda ###

To preserve your native environment, you can run a similar setup through your current anaconda environment.

For instructions on how to install Anaconda, look here: https://docs.anaconda.com/anaconda/install/

Once you have it installed, create an environment for this repo:

```
$ conda create -n haterank
```

Then, install the necessary packages:

```
$ conda install flask
$ conda install matplotlib
$ conda install -c conda-forge tweepy
$ conda install -c https://conda.anaconda.org/sloria textblob
$ python -m textblob.download_corpora
```

### Getting your Twitter API keys ###

In order for the Twitter API to grab tweets you will need to provide it with the proper authentication keys.

You can find and create authentication keys for this script to access twitter through your account by visiting: https://apps.twitter.com/

Add in your respective consumer key and secret and access token and access token secret into a `keys.txt` file, separated by line.

The order should be:
```
Consumer Key
Consumer Secret
Access Token
Access Token Secret
```
## Running ##

### Running the Flask server ###

Navigate to the root folder of the repo (in most cases, this is the HateRank folder).

```
$ FLASK_APP=flask_main.py flask run
```

The first run of this command should be a little slow, as it has to train the classifier if you haven't already
done so. After that, it will load its training data from a saved file automatically so it doesn't re-train itself.

You can access the client side by going to: http://127.0.0.1:5000
inside your browser. If that isn't corrent, just copy the link that is output by the server after running the command.

### Running the backend code ###

Navigate to the classification folder
```
$ python classify.py ../test_data/jaytaylor_tweets.csv
```

### Running the tweet downloader ###

All you should need to do is run (where [[x],[y],[z]] represents a list of twitter handles without the @, that can be a single handle.):

```
python tweet_downloader.py [[x][y][z]]
```
for the program to grab all the selected tweets from the given users and format them each into respective .csv files aptly named `[username]_tweets.csv`.

This will grab the extended tweets if the tweet is truncated, the full tweet if it's a retweet, and will grab separated lists of all hashtags and user mentions
within a given tweet for each tweet, as well as a True/False on whether or not the given tweet was a retweet.

Current .csv format:

[Tweet ID],[Timestamp],[True/False on whether the tweet was a retweet],[Tweet text],[Hashtags],[User Mentions]
